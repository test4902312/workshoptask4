
public class SimpleUnitTest {

    static Simple s = new Simple(10.0,30.0);
    static TestFramework framework = new TestFramework();
    static void checkGetA(){
        framework.assertEquals(s.getA() ,  10.0);
    }

    private static void checkGetB(){
        framework.assertEquals(s.getB() , 30.0);
    }

    static void checkSetB(){
        s.setB(20.0);
        framework.assertEquals(s.getB(), 20.0);
    }

    public static void printReport(){
        framework.printReport();
    }
}
