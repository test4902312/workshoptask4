import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class TestLauncher {
    public static void main(String[] args) throws InvocationTargetException, NoSuchMethodException, IllegalAccessException {
        runTests(SimpleUnitTest.class);
    }

    public static void runTests(Class<?> testClass) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method[] methods = testClass.getDeclaredMethods();
        for (Method method : methods) {
            if (method.getName().startsWith("check")) {
                try {
                    method.setAccessible(true);
                    System.out.println("Running test: " + method.getName());
                    method.invoke(null);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    System.out.println("Test failed due to exception: " + e.getMessage());
                }
            }
        }
        Method printReportMethod = testClass.getMethod("printReport");
        printReportMethod.invoke(null);
    }
}

