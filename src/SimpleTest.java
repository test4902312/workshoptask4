import static org.junit.Assert.*;

public class SimpleTest {

    @org.junit.Test
    public void squareA() {
        Simple s = new Simple(10.0,20.0);
        s.squareA();
        assertEquals(100.0 , s.getA(),0.001);
    }

    @org.junit.Test
    public void getA() {
        Simple s = new Simple(10.0,20.0);
        assertEquals(10.0 , s.getA(),0.001);
    }

    @org.junit.Test
    public void setB() {
        Simple s = new Simple();
        s.setB(25.0);
        assertEquals(25.0 , s.getB(),0.001);
    }

    @org.junit.Test
    public void getB() {
        Simple s = new Simple(10.0,30.0);
        assertEquals(30.0 , s.getB(),0.001);
    }

    @org.junit.Test
    public void testToString() {
        Simple s = new Simple(10.0,20.0);
        assertEquals("(a:10.0, b:20.0)" , s.toString());
    }
}