import java.util.ArrayList;
import java.util.List;

public class TestFramework {

    private List<String> successes;
    private List<String> failures;

    public TestFramework() {
        successes = new ArrayList<>();
        failures = new ArrayList<>();
    }

    public void assertEquals(Object expected, Object actual) {
        if (expected.equals(actual)) {
            System.out.println("Passed");
            successes.add("Assertion passed : expected " + expected + ", actual " + actual);
        } else {
            System.out.println("Failed");
            failures.add("Assertion failed: expected " + expected + ", but found " + actual);
        }
    }

    public void printReport() {
        System.out.println("============");
        System.out.println("Test Report:");
        System.out.println("============");
        System.out.println("Successes: " + successes.size());
        for (String success : successes) {
            System.out.println("[PASS] " + success);
        }
        System.out.println("Failures: " + failures.size());
        for (String failure : failures) {
            System.out.println("[FAIL] " + failure);
        }
    }

}
