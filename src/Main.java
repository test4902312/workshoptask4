import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main  {
    public static void main(String[] args) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        Simple s = new Simple();
        printClassAndName(s);
        getPublicFieldsAndValues(s);
        getPublicAndPrivateFieldsAndValues(s);
        getPublicMethods(s);
        getPrivateMethod(s);
    }

    private static void printClassAndName(Simple s) {
        System.out.println("Class = "+s.getClass());
        System.out.println("Class Name = "+s.getClass().getName());
    }

    private static void getPublicFieldsAndValues(Simple s) throws IllegalAccessException {
        Field fields[] = s.getClass().getFields();
        System.out.println("Public fields count = "+fields.length);
        for(Field f: fields){
            System.out.printf("Field Name = %s, type = %s, value = %s\n",f.getName(),f.getType(), f.getDouble(s));
        }
    }

    private static void getPublicAndPrivateFieldsAndValues(Simple s) throws IllegalAccessException {
        Field fields[] = s.getClass().getDeclaredFields();
        System.out.println("=========================");
        System.out.println("Public and private fields");
        System.out.println("-------------------------");
        System.out.println("All fields count = "+fields.length);
        for(Field f: fields){
            f.setAccessible(true);
            double newValue = f.getDouble(s);
            newValue++;
            f.setDouble(s,newValue);
            System.out.printf("Field Name = %s, type = %s, value = %s\n",f.getName(),f.getType(), f.getDouble(s));
        }
        System.out.println("-------------------------");
    }

    private static void getPublicMethods(Simple s){
        Method m[] = s.getClass().getMethods();
        System.out.println("=========================");
        System.out.println("Public methods");
        System.out.println("-------------------------");
        System.out.println("Public methods count = "+m.length);
        for(Method method: m){
            System.out.printf("Method Name = %s, return type = %s, parameters = %s\n",method.getName(),method.getReturnType(), method.getParameters());
        }
    }

    private static void getPrivateMethod(Simple s) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        System.out.println("=========================");
        System.out.println("Private method");
        System.out.println("-------------------------");
        Method m = s.getClass().getDeclaredMethod("setA", double.class);
        m.setAccessible(true);
        m.invoke(s, 30.0);
        System.out.println(s);
    }
}
